import { verified } from 'data/verified'

export const isVerified = (id: string) => verified.includes(id)
